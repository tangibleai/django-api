pip install -r requirements.txt

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi
python -m spacy download en_core_web_md

python manage.py makemigrations
python manage.py migrate --noinput
python manage.py collectstatic --no-input --clear

# python manage.py makemigrations
# python manage.py migrate --run-syncdb  # syncdb required for sqlite? localhost?
# python manage.py collectstatic --noinput
