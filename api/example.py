from pathlib import Path

print(__file__)
print(__name__)
print("=" * 10)

print(Path(__file__))

print(Path(__file__).resolve())

print(Path(__file__).resolve().parent)
print(Path(__file__).resolve().parent.parent)

BASE_DIR = Path(__file__).resolve().parent.parent

print(BASE_DIR)

print("=" * 10)
print(BASE_DIR / 'data')

# ===============================
print(__name__)
print("=" * 10)

print(f'Path(__file__) = {Path(__file__)}')

print(f'Path(__file__).resolve() = {Path(__file__).resolve()}')

print(f' Path(__file__).resolve().parent = {Path(__file__).resolve().parent}')
print(f'Path(__file__).resolve().parent.parent = {Path(__file__).resolve().parent.parent}')

BASE_DIR = Path(__file__).resolve().parent.parent

print(f'BASE_DIR = {BASE_DIR}')

print("=" * 10)
print(f'BASE_DIR / "data" = {BASE_DIR / "data"}')
