""" Data models and manipulation functions

>>> payload = {
...    "served": True,
...    "clicked": True,
...    "rating": ["-1", "skip", "+1"]
... }

>> requests.post(url, data=payload)
<Response [200]>
>> resp = _
>> resp.json()
[]
>> requests.post(url, json=payload)
<Response [200]>
>> resp = _
>> resp.json()
['wbn001', 'fbl003', 'fbl001', 'fbl019', 'fbl006', 'fbl023', 'fbl005', 'fbl025', 'fbl021', 'fbl017', 'wbn007', 'fbl007', 'wbn008', 'wbn003', '{wbn001}', '{fbl003}', '{fbl001}', 'wbn004', 'vid013']
"""
import requests

from django.db import models
from django.utils import timezone

import time
# Create your models here.


class RecommendationLog(models.Model):
    sent = models.DateTimeField(default=timezone.now)
    user_id = models.TextField(blank=True, null=True)
    video_id = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.user_id


def find_user_ids_that_work():
    user_ids = []
    for (i, user_id) in enumerate(RecommendationLog.objects.values('user_id').distinct()):
        url = f'https://api.onow.com/api/tangibleai/video_engine/{user_id}'
        print(i, user_id)
        resp = requests.get(url)
        videos = resp.text
        if len(videos) > 2:
            print(videos)
            user_ids.append(dict(user_id=resp.text))
        if videos.lstrip().startswith('<'):
            print('Too many requests!')
            break
        time.sleep(2.1)
