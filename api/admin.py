from django.contrib import admin
from .models import RecommendationLog

# Register your models here.
admin.site.register(RecommendationLog)
