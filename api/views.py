# """
# >>> import requests
# >>> url = "https://onow.qary.ai/api/recommendation/"
# >>> resp = requests.post(url, json={"user_id": '4342728885797811', "business type": "restaurant"})
# <Response [201]>
# >>> len(resp.json())
# 4

# >> resp.json()
# {'user_id': '4342728885797811',
#  'video_ids': ['wbn008', 'fbl007', 'wbn007'],
#  'video_ids_v2': ['fbl028', 'wbn002', 'fbl027'],
#  'video_ids_v3': ['wbn002', 'wbn001', 'fbl001']}
# >> resp = requests.post(url, json={"user_id": '4342728885797811', "business type": "restaurant"})
# >> resp.json()
# {'user_id': '4342728885797811',
# 'video_ids': ['fbl021', 'fbl025', 'fbl008'],
# 'video_ids_v2': ['fbl028', 'wbn002', 'fbl027'],
# 'video_ids_v3': ['wbn002', 'fbl008', 'fbl015']}
# >> resp = requests.post(url, json={"user_id": 4342728885797811, "business type": "restaurant"})
# >> resp.json()
# {'user_id': 4342728885797811,
# 'video_ids': ['wbn003', 'fbl017', 'fbl018'],
# 'video_ids_v2': ['fbl028', 'wbn002', 'fbl027'],
# 'video_ids_v3': ['wbn002', 'wbn008', 'wbn004']}
# >> resp = requests.post(url, json={"user_id": 4342728885797811, "business type": "restaurant"})
# {'user_id': 4342728885797811,
# 'video_ids': ['fbl019', 'fbl006', 'wbn001'],
# 'video_ids_v2': ['fbl028', 'wbn002', 'fbl027'],
# 'video_ids_v3': ['wbn002', 'fbl021', 'fbl015']}
# """

import copy
import pandas as pd
import logging
import requests

from pathlib import Path
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from django_api.settings import DATA_DIR
from .models import RecommendationLog
from .tagger import get_similar_videos
from .utils import score_intersection

log = logging.getLogger(__name__)


DEFAULT_QUERY_DICT = {"clicked": True, "rating": ["-1", "skip", "+1"]}
DEFAULT_GET_RESPONSE_DICT = {
    'ERROR': 'This API only accepts POST requests, and endpoint URLs must end with a slash (/)',
    'USAGE':
        r'Use a POST request like: \n'
        r'>>> url = "https://onow.qary.ai/api/recommendation/ \n'  # notice the backslash for post requests
        r'>>> requests.post(url, json={"user_id": 12345678, "business type": "restaurant"}) \n'
}

VIDEO_CATALOG_FILENAME = 'Chatbot Next Step Content-STARRED.csv'
VIDEO_ID_COLUMN_NAME = 'Short Code'


def get_watched_list(user_id, query_dict=None):
    url = f'https://api.onow.com/api/tangibleai/video_engine/{user_id}'
    query_dict = query_dict or {}
    request_json = copy.deepcopy(DEFAULT_QUERY_DICT)
    request_json.update(query_dict)
    resp = requests.post(url, json=request_json)
    try:
        return resp.json()
    except Exception as e:
        print(e)
    return []


def get_random_recommendation(user_id=None, n=3):
    '''get random video_id from csv'''
    if n < 1:
        return []

    filepath = Path(DATA_DIR, VIDEO_CATALOG_FILENAME)

    df = pd.read_csv(filepath, usecols=[VIDEO_ID_COLUMN_NAME])
    df = df.dropna()

    video_ids = df.sample(n)[VIDEO_ID_COLUMN_NAME]

    return list(video_ids)


def filter_watched_videos(video_ids, watched_videos=None, n=3):
    r""" Remove watched_videos from the video_ids list and pad it to len `n` (3) with random video ids

    >>> list(set(range(3)) - set(range(1,5)))
    [0]
    >>> list(set(range(1,5)) - set(range(3)))
    [3, 4]
    >>> filter_watched_videos(video_ids=list(range(3)))
    [0, 1, 2]
    >>> filter_watched_videos(video_ids=list(range(3)), watched_videos=list(range(1,5)))  # doctest: +ELLIPSIS
    [0, '...']
    >>> len(filter_watched_videos(video_ids=list(range(3)), watched_videos=list(range(1,5))))
    3
    """
    if watched_videos:
        video_ids = list(set(list(video_ids)) - set(list(watched_videos)))
    if len(video_ids) < n:
        log.error(f'Extending recommended videos list because only {len(video_ids)} found.')
    return video_ids + get_random_recommendation(n=(n - len(video_ids)))


def get_recommendation(request_tags, n=3):
    r""" Get top n (default=3) video_ids based on occurrence of request_tags in users_type strings

    Input:
      request_tags (set): tags (key-value pairs as strings) associated with user
    """
    filepath = Path(DATA_DIR, 'Chatbot Next Step Content-STARRED.csv')
    df = pd.read_csv(filepath)
    users_type = df['Users Type'].dropna()
    tags_score = {}

    for row in users_type:
        tags2 = set([s.strip() for s in row.split(",")])
        score = score_intersection(request_tags, tags2)
        tags_score[row] = score

    scores = pd.DataFrame(tags_score.items())
    scores.columns = ['tags', 'scores']
    top_scores = scores.nlargest(n, 'scores')
    top_tags = top_scores['tags']
    video_ids = []

    for row in top_tags:
        vid = df.loc[df['Users Type'] == row, VIDEO_ID_COLUMN_NAME].iloc[0]
        video_ids.append(vid)

    return video_ids


class RecommendationApi(APIView):
    """ Respond to web POST requests at onow.qary.ai/api/recommendation/
    """

    def get(self, request, format=None):
        data = DEFAULT_GET_RESPONSE_DICT
        return Response(data)

    def post(self, request, format=None):

        data = {}

        if not request.body:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        body = request.data
        user_id = body.get('user_id')

        random_video_ids = get_random_recommendation(user_id)
        objs = [RecommendationLog(user_id=user_id, video_id=vid) for vid in random_video_ids]
        RecommendationLog.objects.bulk_create(objs)

        watched_videos = get_watched_list(user_id=user_id)
        # tags_request is a set of `str`s that look like set('business_type: food', 'growing: yes')
        tags_request = get_tags(body)
        video_ids_v2 = filter_watched_videos(get_recommendation(tags_request), watched_videos=watched_videos)
        objs = [RecommendationLog(user_id=user_id, video_id=vid) for vid in video_ids_v2]
        RecommendationLog.objects.bulk_create(objs)

        # tags_request = get_tags(body)
        video_ids_v3 = filter_watched_videos(get_similar_videos(tags_request), watched_videos=watched_videos)
        objs = [RecommendationLog(user_id=user_id, video_id=vid) for vid in video_ids_v3]
        RecommendationLog.objects.bulk_create(objs)

        data['user_id'] = user_id
        data['video_ids'] = random_video_ids
        data['video_ids_v2'] = video_ids_v2
        data['video_ids_v3'] = video_ids_v3
        if data:
            return Response(data, status=201)
        return Response(status=status.HTTP_400_BAD_REQUEST)


def get_tags(body):
    r'''get tags from the request body (a json dict)

    Tags are a `set()` of `str`s like `set('business_type: food', 'growing: yes')`
    And they do not include the user_id key-value pair:

    >>> sorted(get_tags(dict(user_id=123, business='food', revenue=1.0)))
    ['business: food', 'revenue: 1.0']
    '''

    # tags are key-value pairs as strings from ONOW, with curly braces, commas, and all
    request_tags = set()

    # filter out the user_id key-value pair while composing a set of strings/tags
    for key in body.keys():
        if key != 'user_id':
            request_tags.add(f'{key}: {body[key]}')

    return request_tags
