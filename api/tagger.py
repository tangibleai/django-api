
from django_api.settings import DATA_DIR
import pandas as pd
from pathlib import Path
import spacy


nlp = spacy.load('en_core_web_md')

VIDEO_CATALOG_FILENAME = 'Chatbot Next Step Content-STARRED.csv'
VIDEO_ID_COLUMN_NAME = 'Short Code'


def to_ascii(s):
    return "".join([c for c in s if ord(c) < 128])


def clean_columns(df):
    df.columns = [to_ascii(c).strip() for c in df.columns]
    return df


filepath = Path(DATA_DIR, VIDEO_CATALOG_FILENAME)
DF_VIDEOS = clean_columns(pd.read_csv(filepath))


def get_similar_videos(user_tags, df_videos=DF_VIDEOS):
    user_tags = str(user_tags).replace('_', ' ')

    df_videos['user_similarity'] = [nlp(user_tags).similarity(nlp(s)) for s in df_videos['Subtext']]
    df_videos = df_videos.sort_values('user_similarity')
    return list(reversed(list(df_videos.tail(3)[VIDEO_ID_COLUMN_NAME])))
