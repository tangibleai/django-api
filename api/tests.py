
from django.test import TestCase
from django.utils import timezone
from rest_framework import status
from rest_framework.test import APITestCase

from .models import RecommendationLog
from .tagger import to_ascii, get_similar_videos
from .utils import score_intersection
from .views import get_tags

import unittest  # noqa
import doctest
from api import views, models, tagger


def load_tests(loader, tests, ignore):
    for module in views, models, tagger:
        tests.addTests(doctest.DocTestSuite(module, optionflags=doctest.ELLIPSIS | doctest.NORMALIZE_WHITESPACE))
    return tests


class RecommendationLogModelTests(TestCase):
    """ tests for RecommendationLog Model """

    def test_recommendation_is_created(self):
        """test RecommendationLog is created"""

        self.assertEqual(RecommendationLog.objects.last(), None)

        timezone_now = timezone.now()
        new_log = RecommendationLog.objects.create(sent=timezone_now,
                                                   user_id='testuser123',
                                                   video_id='wbn007')
        new_log.save()

        self.assertEqual(RecommendationLog.objects.last().sent, timezone_now)
        self.assertEqual(RecommendationLog.objects.last().user_id, 'testuser123')
        self.assertEqual(RecommendationLog.objects.last().video_id, 'wbn007')


class RecommendationTestCase(APITestCase):
    """ test /api/recommendation/ """

    def test_get_recommendation_usage_error(self):
        """ test get request """

        resp = self.client.get('/api/recommendation/')

        self.assertTrue('USAGE' in resp.data)
        self.assertTrue('ERROR' in resp.data)

    def test_post_recommendation(self):
        """ test post request """

        data = {"user_id": "testuser123"}
        resp = self.client.post('/api/recommendation/', data)

        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(resp.data['video_ids']), 3)


class RecomendationV2TestCase(APITestCase):
    """ """

    def test_v2(self):
        data = {
            "user_id": "user321",
            "biz_health_status": "Dying",
            "biz_industry": "Retail",
            "location_group": "Upper Myanmar"
        }
        responce = self.client.post('/api/recommendation/', data)

        self.assertEqual(responce.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(responce.data['video_ids']), 3)
        self.assertEqual(len(responce.data['video_ids_v2']), 3)
        self.assertEqual(len(responce.data['video_ids_v3']), 3)
        self.assertTrue(isinstance(responce.data['video_ids_v2'], list))
        self.assertEqual(responce.data['video_ids_v2'][0], "fbl001")


class GetTagsTests(TestCase):
    """  """

    def test_get_tags(self):
        """test get_tags()"""
        body = {
            "user_id": "<user_id>",
            "biz_health_status": "Growing",
            "biz_industry": "Manufacture"
        }
        tags = get_tags(body)
        self.assertEqual(tags, {
            "biz_industry: Manufacture",
            "biz_health_status: Growing"
        })


class SetIntersectionTests(TestCase):
    """  """

    def test_set_intersection_score(self):
        """test score_intersection """
        tags1 = {
            "biz_industry: Manufacture",
            "biz_health_status: Growing"
        }
        tags2 = {
            "location_group: Upper Myanmar",
            "biz_health_status: Dying",
            "biz_industry: Retail",
            "biz_health_status: Growing"
        }
        score = score_intersection(tags1, tags2)
        self.assertEqual(score, 1)


class ToAsciiTests(TestCase):

    def test_set_to_ascii(self):
        """test convert non ascii string to a string """

        non_ascii_string = '''Subtext 🇬🇧'''
        new_string = to_ascii(non_ascii_string)
        self.assertEqual(non_ascii_string.isascii(), False)
        self.assertEqual(new_string.isascii(), True)


class GetSimilarVideosTests(TestCase):

    def test_get_similar_videos(self):
        """test get_similar_videos """

        user_tags = {'searched business'
                     'biz_health_status: Growing'}
        video_ids_v3 = get_similar_videos(user_tags)
        self.assertEqual(len(video_ids_v3), 3)
        self.assertTrue(isinstance(video_ids_v3, list))
