#!/bin/sh

echo SQL_ENGINE=django.db.backends.postgresql >> .env
echo DATABASE=postgres >> .env
echo VIRTUAL_PORT=8000 >> .env

echo DEBUG=$DEBUG >> .env
echo SECRET_KEY=$SECRET_KEY >> .env
if [ -n "$DJANGO_ALLOWED_HOSTS" ] ; then
    echo "using existing DJANGO_ALLOWED_HOSTS='$DJANGO_ALLOWED_HOSTS'"
else
    export DJANGO_ALLOWED_HOSTS=0.0.0.0 
    echo "Creating DJANGO_ALLOWED_HOSTS='$DJANGO_ALLOWED_HOSTS'"
fi
echo DJANGO_ALLOWED_HOSTS=$DJANGO_ALLOWED_HOSTS  >> .env
echo SQL_DATABASE=$SQL_DATABASE >> .env
echo SQL_USER=$SQL_USER >> .env
echo SQL_PASSWORD=$SQL_PASSWORD >> .env
echo SQL_HOST=$SQL_HOST >> .env
echo SQL_PORT=$SQL_PORT >> .env
echo DEFAULT_EMAIL=$DEFAULT_EMAIL >> .env
echo NGINX_PROXY_CONTAINER=$NGINX_PROXY_CONTAINER >> .env
echo VIRTUAL_HOST=$VIRTUAL_HOST >> .env
echo LETSENCRYPT_HOST=$LETSENCRYPT_HOST >> .env

echo WEB_IMAGE=$IMAGE:web  >> .env
echo NGINX_IMAGE=$IMAGE:nginx  >> .env
echo CI_REGISTRY_USER=$CI_REGISTRY_USER   >> .env
echo CI_JOB_TOKEN=$CI_JOB_TOKEN  >> .env
echo CI_REGISTRY=$CI_REGISTRY  >> .env
echo IMAGE=$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME >> .env

VENV_ACTIVATE_PATH=".venv/bin/activate"
if [ -f "$VENV_ACTIVATE_PATH" ] ; then
    echo "" >> .venv/bin/activate
    echo "source .env" >> .venv/bin/activate
fi
